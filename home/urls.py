from django.conf.urls import url
from django.contrib.auth import views as auth_views

import home.views as views

urlpatterns = [
    url(r'^$', views.index, name='index'),

    # Auth related urls
    url(r'^login/$', auth_views.login, name='login'),
    url(r'^logout/$', auth_views.logout, name='logout'),
]
