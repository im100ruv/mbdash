from django.db import models
from django.contrib.auth.models import User

from mentor.models import Bootcamp, ChecklistTemplate


class UserChecklist(models.Model):
    student = models.ForeignKey(User)
    bootcamp = models.ForeignKey(Bootcamp)
    instructions = models.TextField()
    name = models.CharField(max_length=100)

    def __str__(self):
        return f"<UserChecklist {self.student.username}-{self.name}>"

    @staticmethod
    def create_checklist_from_template(template_id, student_id):
        template = ChecklistTemplate.objects.get(pk=template_id)
        cl = UserChecklist(
            instructions=template.instructions,
            name=template.name,
        )
        cl.student_id = student_id
        cl.bootcamp_id = template.bootcamp_id
        cl.save()
        for item in template.checklistitemtemplate_set.all():
            ChecklistItem.create_from_template(item, cl)
        return cl


class ChecklistItem(models.Model):
    user_checklist = models.ForeignKey(UserChecklist)
    text = models.CharField(max_length=1024)
    is_done = models.BooleanField(default=False)

    def __str__(self):
        return f"<ChecklistItem {self.text}>"

    @staticmethod
    def create_from_template(item_template, cl):
        cli = ChecklistItem(
            text=item_template.text,
            user_checklist=cl,
        )
        print("jdfhdjfhdjfh")
        cli.save()
        return cli
