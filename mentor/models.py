from django.db import models


class Bootcamp(models.Model):
    name = models.CharField(max_length=100)
    description = models.TextField()

    def __str__(self):
        return f"<Bootcamp {self.name}>"


class ChecklistTemplate(models.Model):
    bootcamp = models.ForeignKey(Bootcamp)
    instructions = models.TextField()
    name = models.CharField(max_length=100)

    def __str__(self):
        return f"<ChecklistTemplate {self.name}>"


class ChecklistItemTemplate(models.Model):
    checklist_template = models.ForeignKey(ChecklistTemplate)
    text = models.CharField(max_length=100)

    def __str__(self):
        return f"<ChecklistItemTemplate {self.text}>"
