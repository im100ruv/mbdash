from django.conf.urls import url, include
from rest_framework import routers

from .views import (
    UserChecklistViewSet,
    BootcampViewSet,
    ChecklistItemViewSet
    )


router = routers.DefaultRouter()
router.register(r'checklists', UserChecklistViewSet)
router.register(r'bootcamps', BootcampViewSet)
router.register(r'checklist_items', ChecklistItemViewSet)

urlpatterns = [
    url('^', include(router.urls)),
]
