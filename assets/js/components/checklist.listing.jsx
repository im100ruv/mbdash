import React     from 'react';
import PropTypes from 'prop-types'; 

const ChecklistListing = ({checklists}) => {
  console.log(checklists)
  return (
    checklists.map(c => <h3>{c.name}</h3>)    
  );
};

ChecklistListing.propTypes = {
  // Write your prop types here
};

ChecklistListing.defaultProps = {
  // Write the default values of your props here
};

export default ChecklistListing;
