import ReactDOM from 'react-dom';
import { BrowserRouter } from 'react-router-dom';

import StudentDashComponent from './components/student.dash.jsx';


ReactDOM.render(
  <BrowserRouter>
    <StudentDashComponent />
  </BrowserRouter>,
  document.getElementById('main')
);
console.log("hello world");


