with import <nixpkgs> {};
with pkgs.python36Packages;
with pkgs.nodePackages;

stdenv.mkDerivation rec {

  name = "django-and-node";
  evn = buildEnv { name = name; paths = buildInputs; };

  buildInputs = [
    python36
    python36Packages.django
    python36Packages.djangorestframework
    python36Packages.virtualenvwrapper
    python36Packages.elpy
    # python36Packages.rope
    python36Packages.virtualenv
    python36Packages.jedi
    python36Packages.flake8
    python36Packages.autopep8
    python36Packages.importmagic

    nodejs-8_x
    nodePackages.webpack
  ];


    shellHook = ''

    SOURCE_DATE_EPOCH=$(date +%s)
      export WORKON_HOME=$HOME/.virtualenvs
      source `which virtualenvwrapper.sh`
    '';
}
